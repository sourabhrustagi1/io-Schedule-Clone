package com.example.ioshedclone.di

import android.content.Context
import com.example.ioshedclone.MainApplication
import dagger.Module
import dagger.Provides

@Module
class AppModule {
    @Provides
    fun provideContext(application: MainApplication): Context {
        return application.applicationContext
    }
}