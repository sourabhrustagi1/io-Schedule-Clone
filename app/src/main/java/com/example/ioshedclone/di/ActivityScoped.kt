package com.example.ioshedclone.di

import java.lang.annotation.RetentionPolicy
import javax.inject.Scope
import java.lang.annotation.Documented
import java.lang.annotation.Retention

@Documented
@Scope
@Retention(RetentionPolicy.RUNTIME)
annotation class ActivityScoped
